package cl.demo.rickandmortydemocompose.domain.repository

import cl.demo.rickandmortydemocompose.data.Result
import cl.demo.rickandmortydemocompose.domain.model.Character
import cl.demo.rickandmortydemocompose.domain.model.Characters

interface CharacterRepository {

    suspend fun getCharacters(page: Int): Result<List<Characters>>
    suspend fun getCharacter(id: Int): Result<Character>
}