package cl.demo.rickandmortydemocompose.domain.model

data class Origin(
    val name: String,
    val url: String
)