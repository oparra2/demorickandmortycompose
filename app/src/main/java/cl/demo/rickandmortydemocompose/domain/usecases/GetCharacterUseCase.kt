package cl.demo.rickandmortydemocompose.domain.usecases

import cl.demo.rickandmortydemocompose.data.Result
import cl.demo.rickandmortydemocompose.domain.model.Character
import cl.demo.rickandmortydemocompose.domain.repository.CharacterRepository
import javax.inject.Inject

class GetCharacterUseCase @Inject constructor(
    private val repository: CharacterRepository
) {
    suspend operator fun invoke(id: Int): Result<Character> {
        return repository.getCharacter(id)
    }
}