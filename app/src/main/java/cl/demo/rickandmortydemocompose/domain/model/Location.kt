package cl.demo.rickandmortydemocompose.domain.model

data class Location(
    val name: String,
    val url: String
)