package cl.demo.rickandmortydemocompose.domain.usecases

import cl.demo.rickandmortydemocompose.data.Result
import cl.demo.rickandmortydemocompose.domain.model.Characters
import cl.demo.rickandmortydemocompose.domain.repository.CharacterRepository
import javax.inject.Inject

class GetCharactersUseCase @Inject constructor(
    private val repository: CharacterRepository
) {
    suspend operator fun invoke(page: Int): Result<List<Characters>> {
        return repository.getCharacters(page)
    }
}