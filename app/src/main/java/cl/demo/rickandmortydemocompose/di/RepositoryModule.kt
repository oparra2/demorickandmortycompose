package cl.demo.rickandmortydemocompose.di

import cl.demo.rickandmortydemocompose.domain.repository.CharacterRepository
import cl.demo.rickandmortydemocompose.data.repository.CharacterRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindCharacterRepository(impl: CharacterRepositoryImpl) : CharacterRepository
}