package cl.demo.rickandmortydemocompose.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import cl.demo.rickandmortydemocompose.ui.navigation.AppNavigation
import cl.demo.rickandmortydemocompose.ui.theme.RickAndMortyDemoComposeTheme
import cl.demo.rickandmortydemocompose.ui.viewmodel.CharactersViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RickAndMortyDemoComposeTheme {
                Surface() {
                    AppNavigation()
                }
            }
        }
    }
}