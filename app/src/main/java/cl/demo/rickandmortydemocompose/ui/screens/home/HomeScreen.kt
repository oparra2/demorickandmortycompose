package cl.demo.rickandmortydemocompose.ui.screens.home


import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import cl.demo.rickandmortydemocompose.domain.model.Characters
import cl.demo.rickandmortydemocompose.ui.navigation.AppScreens
import cl.demo.rickandmortydemocompose.ui.viewmodel.CharactersViewModel
import coil.compose.AsyncImage
import kotlinx.coroutines.launch

@Composable
fun HomeScreen(navController: NavController){
    Scaffold(
        topBar = {
            TopAppBar() {
                Spacer(modifier = Modifier.width(10.dp))
                Text(text = "Personajes")
            }
        }
    ) {
        HomeBodyContent(navController)
    }
}

@Composable
fun HomeBodyContent(navController: NavController){
    val viewModel: CharactersViewModel = hiltViewModel()
    val coroutineScope = rememberCoroutineScope()
    var page by rememberSaveable { mutableStateOf(1)}

    Column {
        Row {
            Button(enabled = page > 1,onClick = {
                coroutineScope.launch {
                    page--
                    viewModel.getCharacters(page)
                }
            }) {
                Text("Anterior")
            }
            Button(enabled = page < 42,onClick = {
                coroutineScope.launch {
                    page++
                    viewModel.getCharacters(page)
                }
            }) {
                Text("Siguente")
            }
        }
        LazyColumn(){
            items(viewModel.charactersList.value) { character ->
                CardItem(character, navController)
            }
        }
    }
}


@Composable
fun CardItem(characters: Characters,navController: NavController){
    Card(elevation = 10.dp, modifier = Modifier
        .padding(10.dp)
        .fillMaxWidth()
        .clickable {
            navController.navigate(route = AppScreens.DetailScreen.getId(characters.id))
        }
        ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            AsyncImage(
                model = characters.image,
                contentDescription = characters.name
            )
            Spacer(Modifier.width(10.dp))
            Column() {
                Text(
                    text = characters.name,
                    modifier = Modifier.padding(10.dp)
                )
                Text(
                    text = characters.specie,
                    modifier = Modifier.padding(10.dp)
                )
            }
        }
    }
}