package cl.demo.rickandmortydemocompose.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import cl.demo.rickandmortydemocompose.ui.screens.detail.DetailScreen
import cl.demo.rickandmortydemocompose.ui.screens.home.HomeScreen
import cl.demo.rickandmortydemocompose.ui.screens.intro.IntroScreen

@Composable
fun AppNavigation(){
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = AppScreens.IntroScreen.route){
        composable(route = AppScreens.IntroScreen.route){
            IntroScreen(navController)
        }
        composable(route = AppScreens.HomeScreen.route){
            HomeScreen(navController)
        }
        composable(route = AppScreens.DetailScreen.route, arguments = listOf(navArgument("id") { type = NavType.IntType})){
            val id = it.arguments?.getInt("id")
            requireNotNull(id)
            DetailScreen(navController, id)
        }
    }
}