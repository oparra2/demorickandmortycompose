package cl.demo.rickandmortydemocompose.ui.navigation

sealed class AppScreens(val route: String){
    object IntroScreen: AppScreens("Intro")
    object HomeScreen: AppScreens("Home")
    object DetailScreen: AppScreens("Details/{id}"){
        fun getId(id:Int): String{
            return "Details/$id"
        }
    }
}
