package cl.demo.rickandmortydemocompose.ui.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.demo.rickandmortydemocompose.domain.model.Character
import cl.demo.rickandmortydemocompose.domain.model.Characters
import cl.demo.rickandmortydemocompose.domain.usecases.GetCharacterUseCase
import cl.demo.rickandmortydemocompose.domain.usecases.GetCharactersUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CharactersViewModel @Inject constructor(
    private val charactersUseCase: GetCharactersUseCase,
    private val characterUseCase: GetCharacterUseCase
)  : ViewModel() {

    val charactersList:MutableState<List<Characters>> = mutableStateOf(listOf())
    val character:MutableState<List<Character>> = mutableStateOf(listOf())

    init {
        getCharacters(1)
    }

    fun getCharacters(page:Int){
        viewModelScope.launch {
            val result = charactersUseCase(page)
            if (result.data != null) {
                result.data.let{
                    charactersList.value = it
                }
            } else {
//                msgError.postValue(result.message!!)
            }
        }
    }

    fun getCharacter(id: Int) {
        viewModelScope.launch {
            val result = characterUseCase(id)
            if(result.data != null)     {
                character.value =  listOf(result.data)
            }else {
//                msgError.postValue(result.message!!)
            }
        }
    }


}