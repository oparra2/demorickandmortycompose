package cl.demo.rickandmortydemocompose.ui.screens.intro

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import cl.demo.rickandmortydemocompose.R
import cl.demo.rickandmortydemocompose.ui.navigation.AppScreens

@Composable
fun IntroScreen(navController: NavController){
    Scaffold {
        IntroBodyContent(navController)
    }
}

@Composable
fun IntroBodyContent(navController: NavController){
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(painter = painterResource(R.drawable.logo),"Logo")
        Spacer(modifier = Modifier.height(50.dp))
        Button(onClick = {
            navController.navigate(route = AppScreens.HomeScreen.route)
        }) {
            Text("Ingresar")

        }
    }
}
