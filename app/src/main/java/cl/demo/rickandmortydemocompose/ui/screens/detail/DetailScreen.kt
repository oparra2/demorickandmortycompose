package cl.demo.rickandmortydemocompose.ui.screens.detail

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import cl.demo.rickandmortydemocompose.ui.navigation.AppScreens
import cl.demo.rickandmortydemocompose.ui.viewmodel.CharactersViewModel
import coil.compose.AsyncImage

@Composable
fun DetailScreen(navController: NavController, id: Int){
    Scaffold(
        topBar = {
            TopAppBar() {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = "Volver",
                    modifier = Modifier.clickable { 
                        navController.popBackStack()
                    })
                Spacer(modifier = Modifier.width(10.dp))
                Text(text = "Personaje")
            }
        }
    ) {
        DetailBodyContent(id)
    }
}

@Composable
fun DetailBodyContent(id:Int){
    val viewModel: CharactersViewModel = hiltViewModel()
    viewModel.getCharacter(id)
    Column(
        modifier = Modifier.fillMaxSize(),
    ) {
        viewModel.character.value.forEach { character ->
            Row{
                Card(elevation = 10.dp, modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
                ) {
                    AsyncImage(
                        model = character.image,
                        contentDescription = character.name,
                        modifier = Modifier.size(300.dp,300.dp)
                    )
                }
            }
            Row {
                Card(elevation = 10.dp, modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
                ) {
                    Text(text = character.name)
                }
            }

            Row {
                Card(elevation = 10.dp, modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
                ) {
                    Text(text = character.species)
                }
            }

            Row {
                Card(elevation = 10.dp, modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
                ) {
                    Text(text = character.gender)
                }
            }
            Row {
                Card(elevation = 10.dp, modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
                ) {
                    Text(text = character.status)
                }
            }
        }
    }
}