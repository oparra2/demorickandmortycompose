# android-onboarding
Hola si estás viendo esta página por primera vez, lo más seguro es que necesites un poco de orientación
La idea de este documento es acelerar el proceso de onboarding de nuevos integrantes a la comunidad android de mobdev :)
Me imagino que si llegaste a clonar este repositorio, es por que ya tienes tu cuenta de github creada con el correo de mobdev, por ejemplo name@mobdev.cl.
1. Lo primero que vamos hacer es crear una rama, para ir registrando tus avances.
```sh
git checkout master
git pull
git checkout -b onboarding/add-your-name-and-lastname-here
```
2. Una vez creada tu rama, podrás modificar este documento. A continuación veras unas lista de items, los cuales deberás ir completando, para terminar tu onboarding.
## Kotlin
Si vienes de Java y aún no tienes mucha experiencia, te recomendamos ver estos videos
- [ ] [Kotlin for Java Develpers](https://es.coursera.org/learn/kotlin-for-java-developers)
  - [ ] Week 1
  - [ ] Week 2
  - [ ] Week 3
  - [ ] Week 4
  - [ ] Week 5
- [ ] Kotlin Coroutine
- [ ] Kotlin Flow
- [ ] Kotlin Infix
## Principios S.O.L.I.D
Por acá te recomendamos la siguiente charla
- [ ] [SOLID en Kotlin con Memes](https://web.microsoftstream.com/embed/channel/886cc58a-a56a-44dc-8a43-4341322fcd8c?app=microsoftteams&sort=undefined&l=es-cl#)
## Test y inyección de dependencias
- [ ] [Manual android de Car, Engine intro a inyección de dependencias y test](https://developer.android.com/training/dependency-injection?hl=es-419)
- [ ] Taller de Car, Engine intro a inyección de dependencias y test
- [ ] Crear tu propio taller, por ejemplo con Casa ventana y puerta
## Clean Architecture y inversión de dependencias
- [ ] Taller de clean e inversión de dependencias
## Dagger
- [ ] Taller de intro a Dagger
## Capa Data
- [ ] Taller capa de data
## MVI
- [ ] Taller capa de presentación
## UI
- [ ] Taller de UI
## JetPack Compose
- [ ] Taller JetPack Compose
## Gitflow
- [ ] Git
- [ ] Git flow
- [ ] Git Rebase
## Scrum
- [ ] Taller Ceremonias
## Proyectos internos
- [ ] Padok